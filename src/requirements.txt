# General
pip

# For Postgres
psycopg2-binary
sqlalchemy
sqlalchemy_utils
msgpack

# For RADB
radb

# For MongoDB
pymongo

# For Spark
lxml
