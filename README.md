# COMPSCI 316 Docker

A containerized version of the official course repository at [https://gitlab.oit.duke.edu/compsci316/f2018](https://gitlab.oit.duke.edu/compsci316/f2018).

## Motivation

Setting up VM is frequently frustrating, even with Vagrant and VirtualBox. Since projects in CS316 usually have lots of large dependencies (e.g. PostgreSQL, MongoDB), setting all of them up could be a big hassle. By virtue of containerization, users can expect much fewer configurations, as many of the features come pre-configured with the standard Docker image.

All this containerization does is "paraphrasing" the various shell scripts that were used to provision a usable 316 project system.

## Usage

### Prerequisite

Docker must be installed on the computer that this is run. Follow the instructions on the [official website](https://docs.docker.com/install/linux/docker-ce/debian/) for an installation guide. The linked page is the guide for Debian distributions, but guide for other systems can be found similarly.

In addition, `docker-compose` is an orchestration tool that allows users to start multiple containers at the same time. It may not be bundled with the Docker CE installation, but can be installed separately by following [this guide](https://docs.docker.com/compose/install/).

### Downloading

Once Docker is installed, clone the repository:

```bash
git clone git@gitlab.oit.duke.edu:ym84/cs316docker.git
cd /path/to/cs316docker  # be sure to substitute /path/to/cs316docker with the real path
```

### HW1

Once the data are loaded, we can start the Postgres container, along with the RADB instance:

```bash
sudo docker-compose up postgres
```

Then connect to the RADB container:

```bash
sudo docker-compose run cs316
```

You should get a shell with the user `dbcourse`. If you run `hostname`, it will give you some random characters: that's just the unique identifier Docker created for this container. It's also normal for this identifier to change everything the container is restarted. Now run the `radb` command and connect to the database from another terminal (your first terminal will be stuck at showing Postgres outputs; this is normal):

```bash
radb beers;
```

And hack away!

**Note**: Even though the data in the Postgres database that underlies the RADB container is persistent (i.e. will persist even if container is shut down), rebuilding the container (i.e., running `docker build postgres` or `sudo docker-compose up --build postgres`) will cause the initiailization scripts to be run, which will erase everything, delete the schemas, and recreate them, and populate them with the SQL files again.

### HW2

Only the Postgres container will be used. You might want to mirror / map your local `scripts` directory to the corresponding one in your container, so that you may use `psql` to execute them on the container. To do that, uncomment the line that begins with `/path/to/your/scripts` in the `volumes` directive under the section named `postgres` in `docker-compose.yml`, and replace with the actual path **on the host system**. For example, if the real path is `~/Documents/cs316/scripts`, you will need:

```yaml
volumes:
    - postgresdb-data:/var/lib/postgresql/data
    - ./logs:/var/log/postgresql
    - ~/Documents/cs316/scripts:/scripts  # uncomment this line, too
```

Remember to replace `/path/to/your/scripts` with the actual path on your host computer. Then, run the container and get a shell as usual:

```bash
sudo docker-compose up -d postgres
sudo docker-compose exec --workdir /scripts postgres /bin/bash
```

Now your `/path/to/yours/scripts` directory will be mapped to the `/scripts` directory in the container, which we just specified to be the working direcotry for our Bash shell. Now run a `ls` and make sure your scripts are there!

**Note:** mapping a directory to the container means that changes are propagated *bi-directionally* in real time. What you update on the host computer will be reflected in the container, and vice versa.

### HW3

#### Problem 2

You will need to map the `congress` directory to your container. Uncomment the line under `volumes` in the `cs316` section in `docker-compose.yml`, and replace `/path/to/examples/congress` with the real path on the host system. Then, this should work:

```bash
sudo docker-compose run --workdir /opt/dbcourse/examples/congress cs316
```

By the way, you can get a copy of the course Git repository by simply cloning it:

```bash
git clone git@gitlab.oit.duke.edu:compsci316/f2018.git
```

Then the `examples` directory will be right in it.

The `cs316` container also contains all necessary programs to complete the XC problems :-)

#### Problem 3

The `mongodb` container already contains the necessary initialization scripts to import all data dumps, so it should work right out the box.

```bash
sudo docker-compose up -d mongodb
sudo docker-compose exec mongodb /bin/bash 
```

And to see if it works:

```bash
root@532279ff3f30:/# mongo
> show dbs;
admin     0.000GB
config    0.000GB
congress  0.000GB
local     0.000GB
```

As before, if you need to map a `scirpts` directory from your host system, simply uncomment the line in `volumes` under the `mongodb` section in `docker-compose.yml`.

### Running in background

`docker-compose` also supports running the containers in the background. That is achieved by the `-d` flag:

```bash
sudo docker-compose up -d postgres cs316
# You get your shell back
sudo docker-compose exec cs316
```

### Shutting down

To shut down the containers, simply press Ctrl-c on the first terminal (where you `up`ed the Postgres container).

If you've opted to run the containers in the background, then you will need to navigate to the direcotory that contains the `docker-compose` file, and issue

```bash
sudo docker-compose down
```

## Structure

This project provides a `docker-compose.yml` file for orchestrating the various components of this project, including a Postgres instance. 

### Postgres

The Postgres instance is set up with the following configurations:

* Username: `dbcourse`
* Password: `dbcourse`
* Database name: `beers`

This container's network is isolated from the host because weak passwords are used. This may be a problem for some database visualization tool, such as DataGrip, or even an RADB instance running on the host, since they won't be able to access Postgres. In the case that you must expose this database to the outside world, you can uncomment the ports declaration in `docker-compose.yml`:

```
...
ports:
    - 5432:5432
...
```

Then on your laptop, the Postgres instance is simply running on `localhost`.

### RADB

This container is built on the Anaconda image to provide a usable Python interpreter, and by extension the RADB itself. 

## Additional Containers

* A mongoDB container that supports Homework 3.
* A PHP container that may be used later is added to the `docker-compose.yml` file. However, as long as you don't explicitly start it, it won't be running.