FROM continuumio/anaconda3

# Copy files and housekeeping
RUN mkdir -p /opt/dbcourse
RUN useradd dbcourse
RUN mkdir -p /home/dbcourse
RUN chown dbcourse:dbcourse /home/dbcourse
ADD --chown=dbcourse:dbcourse src/requirements.txt /opt/dbcourse/requirements.txt
ADD --chown=dbcourse:dbcourse src/dotfiles/radb.ini /home/dbcourse/.radb.ini

# Install all Python requirements
RUN pip install --upgrade -r /opt/dbcourse/requirements.txt

# Install Java & Spark
RUN apt-get update && apt-get -qq --yes install \
    openjdk-8-jdk \
    libpg-java \
    libxml2-dev libxslt1-dev libxml2-utils \
    libsaxonb-java

USER dbcourse
WORKDIR /home/dbcourse

ENTRYPOINT /bin/bash
